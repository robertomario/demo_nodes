/**
 * Define node
 * that receives message with key pressed
 * and reacts accordingly
 */

#include "ros/ros.h"
#include "std_msgs/String.h"

float linear_speed{10.0};
float angular_speed{5.0};

void demoCallback(const std_msgs::String::ConstPtr &msg)
{
    std::string *m;
    m = msg->data.c_str();
    ROS_INFO("I heard: [%s]", m);
    if (m == "w")
    {
        linear_speed += 0.5;
        ROS_INFO("Linear speed incremented to %f", angular_speed);
    }
    else if (m == "a")
    {
        angular_speed -= 0.5;
        ROS_INFO("Angular speed reduced to %f", angular_speed);
    }
    else if (m == "s")
    {
        linear_speed -= 0.5;
        ROS_INFO("Linear speed reduced to %f", linear_speed);
    }
    else if (m == "d")
    {
        angular_speed += 0.5;
        ROS_INFO("Angular speed incremented to %f", angular_speed);
    }
    else if (m == "up")
    {
        ROS_INFO("Move forward");
    }
    else if (m == "down")
    {
        ROS_INFO("Move backward");
    }
    else if (m == "left")
    {
        ROS_INFO("Rotate left");
    }
    else if (m == "right")
    {
        ROS_INFO("Rotate right");
    }
    else
    {
        ROS_INFO("Undefined command");
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "subscriber_demo_node");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("demo_topic", 1000, demoCallback);
    ros::spin();
    return 0;
}