/**
 * Define node
 * that detects keys being pressed
 * and sends messages accordingly
 */

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <conio.h>

#define KEY_UP 72
#define KEY_DOWN 80
#define KEY_LEFT 77
#define KEY_RIGHT 75

int main(int argc, char **argv)
{
    ros::init(argc, argv, "publisher_demo_node");
    ros::NodeHandle n;
    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("demo_topic", 1000);
    ros::Rate loop_rate(10);

    while (ros::ok())
    {
        std_msgs::String msg;
        std::stringstream ss;
        char c = getch();
        if (c == 0)
        {
            switch (getch())
            {
            // special KEY_ handling here
            case KEY_UP:
                ss << "up";
                break;
            case KEY_DOWN:
                ss << "down";
                break;
            case KEY_LEFT:
                ss << "left";
                break;
            case KEY_RIGHT:
                ss << "right";
                break;
            default:
                ss << "p";
                break;
            }
        }
        else
        {
            ss << c;
        }
        msg.data = ss.str();
        ROS_INFO("%s", msg.data.c_str());
        chatter_pub.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
    }

    return 0;
}